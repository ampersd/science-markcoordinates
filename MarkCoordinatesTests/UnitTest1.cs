﻿

namespace MarkCoordinatesTests
{
    using MarkCoordinates.Logic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Drawing;

    using TestConsole;

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Arrange
            var cl = new Cluster();

            // Act
            cl.AddPointF(new PointF(1.0f, 2.0f));

            // Assert
            Assert.AreEqual(cl.Center.X, 1.0f);
            Assert.AreEqual(cl.Center.Y, 2.0f);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Assert.AreEqual(5 + 4, 9);
        }
    }
}
