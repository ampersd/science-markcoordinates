﻿namespace TestConsole
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    internal class Program
    {
        internal static void Main(string[] args)
        {            
            int linqCounter = 0;
            var source = new List<byte> { 0, 0, 1, 0, 1 };

            var bytes = source.Where(x =>
            {
                linqCounter++;
                return x > 0;
            });

            if (bytes.First() == bytes.Last())
            {
                Console.WriteLine(linqCounter--);
            }
            else
            {
                Console.WriteLine(linqCounter++);
            }
        }
    }    
}
