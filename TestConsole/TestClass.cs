﻿namespace TestConsole
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;

    public static class TestClass
    {
        private static int[][] matrix = new int[2][];

         static TestClass()
        {
            matrix[0] = new int[2];
            matrix[1] = new int[2];

            matrix[0][0] = 1;
            matrix[0][1] = 0;
            matrix[1][0] = 0;
            matrix[1][1] = 1;
        }

        public static void StreamReader()
        {
            // TODO: понять как можно указать путь к файлу, находящемуся в папке проекта
            string datafile = @"data.txt";
            using (StreamReader reader = File.OpenText(datafile))
            {
                string line = null;
                do
                {
                    line = reader.ReadLine();

                    Console.WriteLine(line);
                }
                while (line != null);
            }

            Console.ReadKey();
        }

        public static void StreamWriter()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(int[][]));
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                serializer.Serialize(sw, matrix);
                Console.WriteLine(Environment.NewLine + "XML: " + Environment.NewLine + sb.ToString());
                Console.ReadKey();
            }
        }              
    }
}