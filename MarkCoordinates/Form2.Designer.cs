﻿namespace MarkCoordinates
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OpenImageBtn = new System.Windows.Forms.Button();
            this.UseDefaultButton = new System.Windows.Forms.Button();
            this.BWButton = new System.Windows.Forms.Button();
            this.BinaryButton = new System.Windows.Forms.Button();
            this.ClusterButton = new System.Windows.Forms.Button();
            this.GetDeformatons = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.MatTest = new System.Windows.Forms.Button();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.ExcelButton = new System.Windows.Forms.Button();
            this.ShowClustersButton = new System.Windows.Forms.Button();
            this.MinPixelsThresholdTextBox = new System.Windows.Forms.TextBox();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.DrawWithColorButton = new System.Windows.Forms.Button();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.u33 = new System.Windows.Forms.TextBox();
            this.u23 = new System.Windows.Forms.TextBox();
            this.u13 = new System.Windows.Forms.TextBox();
            this.u32 = new System.Windows.Forms.TextBox();
            this.u31 = new System.Windows.Forms.TextBox();
            this.u22 = new System.Windows.Forms.TextBox();
            this.Copy2ClipboardButton = new System.Windows.Forms.Button();
            this.u21 = new System.Windows.Forms.TextBox();
            this.u12 = new System.Windows.Forms.TextBox();
            this.u11 = new System.Windows.Forms.TextBox();
            this.surfButton = new System.Windows.Forms.Button();
            this.timeLabel = new System.Windows.Forms.Label();
            this.checkedListBoxFiles = new System.Windows.Forms.CheckedListBox();
            this.radioButtonFiles = new System.Windows.Forms.RadioButton();
            this.radioButtonConfig = new System.Windows.Forms.RadioButton();
            this.groupBoxImages = new System.Windows.Forms.GroupBox();
            this.tbFolderPath = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBoxImages.SuspendLayout();
            this.SuspendLayout();
            // 
            // OpenImageBtn
            // 
            this.OpenImageBtn.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.OpenImageBtn.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OpenImageBtn.Location = new System.Drawing.Point(20, 501);
            this.OpenImageBtn.Name = "OpenImageBtn";
            this.OpenImageBtn.Size = new System.Drawing.Size(128, 36);
            this.OpenImageBtn.TabIndex = 4;
            this.OpenImageBtn.Text = "Open Image";
            this.OpenImageBtn.UseVisualStyleBackColor = true;
            this.OpenImageBtn.Click += new System.EventHandler(this.OpenImageBtn_Click);
            // 
            // UseDefaultButton
            // 
            this.UseDefaultButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.UseDefaultButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UseDefaultButton.Location = new System.Drawing.Point(190, 501);
            this.UseDefaultButton.Name = "UseDefaultButton";
            this.UseDefaultButton.Size = new System.Drawing.Size(128, 36);
            this.UseDefaultButton.TabIndex = 5;
            this.UseDefaultButton.Text = "Use Default";
            this.UseDefaultButton.UseVisualStyleBackColor = true;
            this.UseDefaultButton.Click += new System.EventHandler(this.UseDefaultButton_Click);
            // 
            // BWButton
            // 
            this.BWButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.BWButton.Enabled = false;
            this.BWButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BWButton.Location = new System.Drawing.Point(20, 552);
            this.BWButton.Name = "BWButton";
            this.BWButton.Size = new System.Drawing.Size(128, 36);
            this.BWButton.TabIndex = 7;
            this.BWButton.Text = "Make It BW";
            this.BWButton.UseVisualStyleBackColor = true;
            this.BWButton.Click += new System.EventHandler(this.BWButton_Click);
            // 
            // BinaryButton
            // 
            this.BinaryButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.BinaryButton.Enabled = false;
            this.BinaryButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BinaryButton.Location = new System.Drawing.Point(20, 603);
            this.BinaryButton.Name = "BinaryButton";
            this.BinaryButton.Size = new System.Drawing.Size(128, 36);
            this.BinaryButton.TabIndex = 8;
            this.BinaryButton.Text = "Make It Binary";
            this.BinaryButton.UseVisualStyleBackColor = true;
            this.BinaryButton.Click += new System.EventHandler(this.BinaryButton_Click);
            // 
            // ClusterButton
            // 
            this.ClusterButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.ClusterButton.Enabled = false;
            this.ClusterButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClusterButton.Location = new System.Drawing.Point(190, 603);
            this.ClusterButton.Name = "ClusterButton";
            this.ClusterButton.Size = new System.Drawing.Size(128, 36);
            this.ClusterButton.TabIndex = 9;
            this.ClusterButton.Text = "ClusterList";
            this.ClusterButton.UseVisualStyleBackColor = true;
            this.ClusterButton.Click += new System.EventHandler(this.ClusterButton_Click);
            // 
            // GetDeformatons
            // 
            this.GetDeformatons.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.GetDeformatons.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GetDeformatons.Location = new System.Drawing.Point(492, 501);
            this.GetDeformatons.Name = "GetDeformatons";
            this.GetDeformatons.Size = new System.Drawing.Size(172, 36);
            this.GetDeformatons.TabIndex = 14;
            this.GetDeformatons.Text = "GetDeformations";
            this.GetDeformatons.UseVisualStyleBackColor = true;
            this.GetDeformatons.Click += new System.EventHandler(this.GetDeformations_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(679, 501);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(172, 134);
            this.listBox1.TabIndex = 15;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // MatTest
            // 
            this.MatTest.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.MatTest.Enabled = false;
            this.MatTest.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MatTest.Location = new System.Drawing.Point(343, 501);
            this.MatTest.Name = "MatTest";
            this.MatTest.Size = new System.Drawing.Size(128, 36);
            this.MatTest.TabIndex = 5;
            this.MatTest.Text = "MatTest";
            this.MatTest.UseVisualStyleBackColor = true;
            this.MatTest.Click += new System.EventHandler(this.MatTest_Click);
            // 
            // imageBox2
            // 
            this.imageBox2.InitialImage = global::MarkCoordinates.Properties.Resources.IMG_0681;
            this.imageBox2.Location = new System.Drawing.Point(517, 12);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(719, 469);
            this.imageBox2.TabIndex = 11;
            this.imageBox2.TabStop = false;
            // 
            // imageBox1
            // 
            this.imageBox1.InitialImage = global::MarkCoordinates.Properties.Resources.IMG_0681;
            this.imageBox1.Location = new System.Drawing.Point(20, 12);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(474, 469);
            this.imageBox1.TabIndex = 10;
            this.imageBox1.TabStop = false;
            // 
            // listBox2
            // 
            this.listBox2.Enabled = false;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Items.AddRange(new object[] {
            "Step 1: (R) Rotation",
            "Step 2: (O^-1) Go to def axes",
            "Step 3: (D) Deformation",
            "Step 4: (O1) Returning back to main axes",
            "Step 5: (T) Translation"});
            this.listBox2.Location = new System.Drawing.Point(869, 501);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(172, 134);
            this.listBox2.TabIndex = 16;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // ExcelButton
            // 
            this.ExcelButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.ExcelButton.Enabled = false;
            this.ExcelButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExcelButton.Location = new System.Drawing.Point(679, 644);
            this.ExcelButton.Name = "ExcelButton";
            this.ExcelButton.Size = new System.Drawing.Size(172, 36);
            this.ExcelButton.TabIndex = 14;
            this.ExcelButton.Text = "ExportResult2Excel";
            this.ExcelButton.UseVisualStyleBackColor = true;
            this.ExcelButton.Click += new System.EventHandler(this.ExcelButton_Click);
            // 
            // ShowClustersButton
            // 
            this.ShowClustersButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.ShowClustersButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ShowClustersButton.Location = new System.Drawing.Point(343, 603);
            this.ShowClustersButton.Name = "ShowClustersButton";
            this.ShowClustersButton.Size = new System.Drawing.Size(128, 36);
            this.ShowClustersButton.TabIndex = 9;
            this.ShowClustersButton.Text = "ShowClusters";
            this.ShowClustersButton.UseVisualStyleBackColor = true;
            this.ShowClustersButton.Click += new System.EventHandler(this.ShowClustersButton_Click);
            // 
            // MinPixelsThresholdTextBox
            // 
            this.MinPixelsThresholdTextBox.Location = new System.Drawing.Point(190, 552);
            this.MinPixelsThresholdTextBox.Name = "MinPixelsThresholdTextBox";
            this.MinPixelsThresholdTextBox.Size = new System.Drawing.Size(281, 20);
            this.MinPixelsThresholdTextBox.TabIndex = 17;
            this.MinPixelsThresholdTextBox.Text = "100";
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Location = new System.Drawing.Point(343, 579);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(40, 13);
            this.ResultLabel.TabIndex = 18;
            this.ResultLabel.Text = "Result:";
            // 
            // DrawWithColorButton
            // 
            this.DrawWithColorButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.DrawWithColorButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DrawWithColorButton.Location = new System.Drawing.Point(190, 754);
            this.DrawWithColorButton.Name = "DrawWithColorButton";
            this.DrawWithColorButton.Size = new System.Drawing.Size(172, 36);
            this.DrawWithColorButton.TabIndex = 9;
            this.DrawWithColorButton.Text = "DrawPointsWithColor";
            this.DrawWithColorButton.UseVisualStyleBackColor = true;
            this.DrawWithColorButton.Click += new System.EventHandler(this.DrawWithColorButton_Click);
            // 
            // listBox3
            // 
            this.listBox3.Enabled = false;
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Items.AddRange(new object[] {
            "matrix"});
            this.listBox3.Location = new System.Drawing.Point(1064, 501);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(172, 134);
            this.listBox3.TabIndex = 16;
            this.listBox3.SelectedIndexChanged += new System.EventHandler(this.listBox3_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.u33);
            this.panel1.Controls.Add(this.u23);
            this.panel1.Controls.Add(this.u13);
            this.panel1.Controls.Add(this.u32);
            this.panel1.Controls.Add(this.u31);
            this.panel1.Controls.Add(this.u22);
            this.panel1.Controls.Add(this.Copy2ClipboardButton);
            this.panel1.Controls.Add(this.u21);
            this.panel1.Controls.Add(this.u12);
            this.panel1.Controls.Add(this.u11);
            this.panel1.Location = new System.Drawing.Point(869, 644);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(367, 172);
            this.panel1.TabIndex = 19;
            // 
            // u33
            // 
            this.u33.Location = new System.Drawing.Point(246, 89);
            this.u33.Name = "u33";
            this.u33.Size = new System.Drawing.Size(100, 20);
            this.u33.TabIndex = 0;
            // 
            // u23
            // 
            this.u23.Location = new System.Drawing.Point(246, 52);
            this.u23.Name = "u23";
            this.u23.Size = new System.Drawing.Size(100, 20);
            this.u23.TabIndex = 0;
            // 
            // u13
            // 
            this.u13.Location = new System.Drawing.Point(246, 16);
            this.u13.Name = "u13";
            this.u13.Size = new System.Drawing.Size(100, 20);
            this.u13.TabIndex = 0;
            // 
            // u32
            // 
            this.u32.Location = new System.Drawing.Point(130, 89);
            this.u32.Name = "u32";
            this.u32.Size = new System.Drawing.Size(100, 20);
            this.u32.TabIndex = 0;
            // 
            // u31
            // 
            this.u31.Location = new System.Drawing.Point(15, 89);
            this.u31.Name = "u31";
            this.u31.Size = new System.Drawing.Size(100, 20);
            this.u31.TabIndex = 0;
            // 
            // u22
            // 
            this.u22.Location = new System.Drawing.Point(130, 52);
            this.u22.Name = "u22";
            this.u22.Size = new System.Drawing.Size(100, 20);
            this.u22.TabIndex = 0;
            // 
            // Copy2ClipboardButton
            // 
            this.Copy2ClipboardButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.Copy2ClipboardButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Copy2ClipboardButton.Location = new System.Drawing.Point(15, 124);
            this.Copy2ClipboardButton.Name = "Copy2ClipboardButton";
            this.Copy2ClipboardButton.Size = new System.Drawing.Size(331, 36);
            this.Copy2ClipboardButton.TabIndex = 14;
            this.Copy2ClipboardButton.Text = "Copy2Clipboard";
            this.Copy2ClipboardButton.UseVisualStyleBackColor = true;
            this.Copy2ClipboardButton.Click += new System.EventHandler(this.Copy2ClipboardButton_Click);
            // 
            // u21
            // 
            this.u21.Location = new System.Drawing.Point(15, 52);
            this.u21.Name = "u21";
            this.u21.Size = new System.Drawing.Size(100, 20);
            this.u21.TabIndex = 0;
            // 
            // u12
            // 
            this.u12.Location = new System.Drawing.Point(130, 16);
            this.u12.Name = "u12";
            this.u12.Size = new System.Drawing.Size(100, 20);
            this.u12.TabIndex = 0;
            // 
            // u11
            // 
            this.u11.Location = new System.Drawing.Point(15, 16);
            this.u11.Name = "u11";
            this.u11.Size = new System.Drawing.Size(100, 20);
            this.u11.TabIndex = 0;
            // 
            // surfButton
            // 
            this.surfButton.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.surfButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.surfButton.Location = new System.Drawing.Point(20, 754);
            this.surfButton.Name = "surfButton";
            this.surfButton.Size = new System.Drawing.Size(128, 36);
            this.surfButton.TabIndex = 8;
            this.surfButton.Text = "SURF";
            this.surfButton.UseVisualStyleBackColor = true;
            this.surfButton.Click += new System.EventHandler(this.surfButton_Click);
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(20, 735);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(70, 13);
            this.timeLabel.TabIndex = 20;
            this.timeLabel.Text = "Elapsed time:";
            // 
            // checkedListBoxFiles
            // 
            this.checkedListBoxFiles.CheckOnClick = true;
            this.checkedListBoxFiles.Enabled = false;
            this.checkedListBoxFiles.FormattingEnabled = true;
            this.checkedListBoxFiles.Location = new System.Drawing.Point(368, 666);
            this.checkedListBoxFiles.Name = "checkedListBoxFiles";
            this.checkedListBoxFiles.Size = new System.Drawing.Size(296, 124);
            this.checkedListBoxFiles.TabIndex = 21;
            // 
            // radioButtonFiles
            // 
            this.radioButtonFiles.AutoSize = true;
            this.radioButtonFiles.Location = new System.Drawing.Point(16, 45);
            this.radioButtonFiles.Name = "radioButtonFiles";
            this.radioButtonFiles.Size = new System.Drawing.Size(51, 17);
            this.radioButtonFiles.TabIndex = 0;
            this.radioButtonFiles.Text = "folder";
            this.radioButtonFiles.UseVisualStyleBackColor = true;
            this.radioButtonFiles.CheckedChanged += new System.EventHandler(this.radioButtonFiles_CheckedChanged);
            // 
            // radioButtonConfig
            // 
            this.radioButtonConfig.AutoSize = true;
            this.radioButtonConfig.Checked = true;
            this.radioButtonConfig.Location = new System.Drawing.Point(16, 19);
            this.radioButtonConfig.Name = "radioButtonConfig";
            this.radioButtonConfig.Size = new System.Drawing.Size(54, 17);
            this.radioButtonConfig.TabIndex = 0;
            this.radioButtonConfig.TabStop = true;
            this.radioButtonConfig.Text = "config";
            this.radioButtonConfig.UseVisualStyleBackColor = true;
            // 
            // groupBoxImages
            // 
            this.groupBoxImages.Controls.Add(this.tbFolderPath);
            this.groupBoxImages.Controls.Add(this.radioButtonFiles);
            this.groupBoxImages.Controls.Add(this.radioButtonConfig);
            this.groupBoxImages.Location = new System.Drawing.Point(492, 552);
            this.groupBoxImages.Name = "groupBoxImages";
            this.groupBoxImages.Size = new System.Drawing.Size(172, 102);
            this.groupBoxImages.TabIndex = 23;
            this.groupBoxImages.TabStop = false;
            this.groupBoxImages.Text = "Images from...";
            // 
            // tbFolderPath
            // 
            this.tbFolderPath.Location = new System.Drawing.Point(6, 68);
            this.tbFolderPath.Name = "tbFolderPath";
            this.tbFolderPath.Size = new System.Drawing.Size(160, 20);
            this.tbFolderPath.TabIndex = 1;
            this.tbFolderPath.Text = "..\\..\\..\\ExperimentalData\\step6 - increasing tiny points\\";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 828);
            this.Controls.Add(this.groupBoxImages);
            this.Controls.Add(this.checkedListBoxFiles);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.MinPixelsThresholdTextBox);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.ExcelButton);
            this.Controls.Add(this.GetDeformatons);
            this.Controls.Add(this.imageBox2);
            this.Controls.Add(this.imageBox1);
            this.Controls.Add(this.DrawWithColorButton);
            this.Controls.Add(this.ShowClustersButton);
            this.Controls.Add(this.ClusterButton);
            this.Controls.Add(this.surfButton);
            this.Controls.Add(this.BinaryButton);
            this.Controls.Add(this.BWButton);
            this.Controls.Add(this.MatTest);
            this.Controls.Add(this.UseDefaultButton);
            this.Controls.Add(this.OpenImageBtn);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxImages.ResumeLayout(false);
            this.groupBoxImages.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpenImageBtn;
        private System.Windows.Forms.Button UseDefaultButton;
        private System.Windows.Forms.Button BWButton;
        private System.Windows.Forms.Button BinaryButton;
        private System.Windows.Forms.Button ClusterButton;
        private Emgu.CV.UI.ImageBox imageBox1;
        private Emgu.CV.UI.ImageBox imageBox2;
        private System.Windows.Forms.Button GetDeformatons;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button MatTest;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button ExcelButton;
        private System.Windows.Forms.Button ShowClustersButton;
        private System.Windows.Forms.TextBox MinPixelsThresholdTextBox;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.Button DrawWithColorButton;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox u33;
        private System.Windows.Forms.TextBox u23;
        private System.Windows.Forms.TextBox u13;
        private System.Windows.Forms.TextBox u32;
        private System.Windows.Forms.TextBox u31;
        private System.Windows.Forms.TextBox u22;
        private System.Windows.Forms.Button Copy2ClipboardButton;
        private System.Windows.Forms.TextBox u21;
        private System.Windows.Forms.TextBox u12;
        private System.Windows.Forms.TextBox u11;
        private System.Windows.Forms.Button surfButton;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.CheckedListBox checkedListBoxFiles;
        private System.Windows.Forms.RadioButton radioButtonFiles;
        private System.Windows.Forms.RadioButton radioButtonConfig;
        private System.Windows.Forms.GroupBox groupBoxImages;
        private System.Windows.Forms.TextBox tbFolderPath;
    }
}