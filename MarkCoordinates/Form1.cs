﻿namespace MarkCoordinates
{
    using System;
    using System.IO;
    using System.Windows.Forms;
    using MarkCoordinates.Logic;

    /// <summary>
    /// Form class.
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1Click(object sender, EventArgs e)
        {
            var openfile = new OpenFileDialog();
            if (openfile.ShowDialog() == DialogResult.OK)
            {
                const int Scale = 1;
                var points = Workflow.GetPointsFromImage(openfile.FileName, Scale, imageBox1, imageBox2);
            }
        }

        

        private void Test_Click(object sender, EventArgs e)
        {
            const int Scale = 1;
            var points1 = Workflow.GetPointsFromImage(@"..\..\..\images\IMG_0682.jpg", Scale, imageBox1, imageBox2);
            var points2 = Workflow.GetPointsFromImage(@"..\..\..\images\IMG_0683.jpg", Scale, imageBox1, imageBox2);
            var matOps = new MatrixOperations(new Points(points1, points2), infoTextBox);
            matOps.UltimateWorkflow();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            var saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                            
                this.imageBox2.Image.Save(saveFileDialog1.FileName);

            }

        }

        private void Export2Desktop_Click(object sender, EventArgs e)
        {
            var text = infoTextBox.Text;
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            using (StreamWriter outfile = new StreamWriter(path + @"\result.txt"))
            {
                outfile.Write(text);
            }
        }     
    }
}
