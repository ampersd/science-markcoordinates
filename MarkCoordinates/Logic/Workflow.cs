﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkCoordinates.Logic
{
    using System.Drawing;

    using Emgu.CV;
    using Emgu.CV.CvEnum;
    using Emgu.CV.Structure;
    using Emgu.CV.UI;


    // Тут должна быть вся сторонняя логика из Form1.cs - принцип разделения отрисовки и бизнес-логики
    static class Workflow
    {
        public static List<Point> GetPointsFromImage(string filename, int Scale, ImageBox imageBox1, ImageBox imageBox2)
        {
            var orImage = new Image<Gray, Byte>(filename);
            orImage = orImage.Resize(orImage.Width / Scale, orImage.Height / Scale, INTER.CV_INTER_LINEAR);
            var bwImage = orImage.Convert<Gray, Byte>();
            var binaryImage = bwImage.ThresholdBinary(new Gray(127.0), new Gray(255.0));
            return GetPointsFromImage(binaryImage, Scale, imageBox1, imageBox2);
        }        
        
        public static List<Point> GetPointsFromImage(Image<Gray, Byte> binImage, int Scale, ImageBox imageBox1, ImageBox imageBox2)
        {
            var clusterList = new ClusterList(binImage);
            clusterList.UltimateWorkflow();
            // 0 - начиная с какого индекса, 3 - сколько точек
            var centerPoints = clusterList.IntCenterPoints;
            var points = GetPoints(centerPoints, 0, centerPoints.Count);

            //  Output image
            var outImage = new Image<Bgr, Byte>(binImage.Width, binImage.Height);
            outImage._Not();
            outImage.DrawContours(clusterList.AllContours);
            outImage.DrawCenterPoints(clusterList.CenterPointsF);
            ////outImage.DrawLines(clusterList.LineSegments, clusterList.Scale);
            ////outImage.DrawCalibrationRect(clusterList.Rect, clusterList.Scale);

            ////var clusterImage = new Image<Bgr, Byte>(binImage.Width, binImage.Height);
            ////clusterImage._Not();
            ////clusterImage.DrawFilledContours(clusterList.AllContours);

            imageBox1.Image = binImage;
            imageBox2.Image = outImage;
            var intPoints = new List<Point>();
            foreach (var point in points)
            {
                intPoints.Add(point);
            }
            return intPoints;
        }
        
        public static List<Point> GetPoints(List<Point> centerPoints, int begin, int count)
        {
            if (centerPoints != null)
            {
                return centerPoints.GetRange(begin, count);
            }
            return new List<Point>();
        }

        ////public static List<Point> SortPoints(List<Point> intPoints, int columns)
        ////{
        ////    //  сначала сортируем по Y координате, затем группируем по делению на количество столбцов, которые хотим получить (скажем, 5 столбцов)
        ////    //  далее для каждой группы (допустим в итоге у нас три группы) мы производим свою сортировку по X координате
        ////    //  в итоге sortedPoints содержит отсортированные сначала по Y, а затем по X элементы
        ////    var helpPoints = intPoints.OrderBy(p => p.Y)
        ////                                .Select((value, index) => new { index, value })
        ////                                .GroupBy(element => element.index / columns)
        ////                                .ToList();
        ////    var sortedPoints = new List<Point>();
        ////    foreach (var group in helpPoints)
        ////    {
        ////        var sortedGroup = group.OrderBy(element => element.value.X).ToList();

        ////        foreach (var element in sortedGroup)
        ////        {
        ////            sortedPoints.Add(element.value);
        ////        }
        ////    }
        ////    return sortedPoints;
        ////}


    }
}
