﻿namespace MarkCoordinates.Logic
{
    ////using System;
    ////using System.Collections.Generic;
    ////using System.Drawing;
    ////using System.Linq;

    ////using Emgu.CV;
    ////using Emgu.CV.CvEnum;
    ////using Emgu.CV.Structure;

    /////// <summary>
    /////// Not sure yet if this class needs to look like this.
    /////// </summary>
    ////internal static class BitmapExtensions
    ////{
    ////    /// <summary>
    ////    /// Extension method for Bitmap. Implements lines allocation from image.
    ////    /// </summary>
    ////    /// <param name="bitmap">This variable will contain the result.</param>
    ////    /// <param name="fileName">This is the filename of image on the hard disk.</param>
    ////    /// <example>                         
    ////    ///     var result = new Bitmap(size, size);
    ////    ///     result.CannyLines(openfile.FileName);
    ////    /// </example>
    ////    internal static void CannyLines(this Bitmap bitmap, String fileName)
    ////    {
    ////        // Load the image from file and resize it for display
    ////        var img = new Image<Bgr, byte>(fileName)
    ////                                   .Resize(400, 400, INTER.CV_INTER_LINEAR, true);

    ////        // Convert the image to grayscale
    ////        Image<Gray, Byte> gray = img.Convert<Gray, Byte>();
            
    ////        Image<Gray, Byte> cannyEdges = ImageProcessing.CannyFromBgr(img);
    ////        LineSegment2D[] lines = ImageProcessing.GetLinesFromCanny(cannyEdges);
            
    ////        // draw lines
    ////        Image<Bgr, Byte> lineImage = img.CopyBlank();
    ////        lineImage.DrawLines(lines);

    ////        var centers = lines.Centers(3);
    ////        foreach (PointF center in centers)
    ////        {
    ////            lineImage.Draw(new CircleF(center, 4), new Bgr(Color.Green), 2);
    ////        }
            
    ////        bitmap = lineImage.ToBitmap();
    ////    }
    ////}
}
