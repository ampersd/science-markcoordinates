﻿namespace MarkCoordinates.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Emgu.CV;
    using Emgu.CV.CvEnum;
    using Emgu.CV.Structure;

    internal static class LinesExtensions
    {
        /// <summary>
        /// Method using K-means clustering.
        /// </summary>
        /// <param name="lines">List of lines comes from houghlines algorithm.</param>
        /// <param name="numberOfClusters">How much clusters do we want.</param>
        /// <returns>Set of centers points.</returns>
        public static IEnumerable<PointF> Centers(this IList<LineSegment2D> lines, Int32 numberOfClusters)
        {
            Int32 numberOfPoints = lines.Count() * 2;
            Int32 clusterCount;
            if (numberOfClusters > numberOfPoints)
            {
                clusterCount = numberOfPoints - 1 >= 0 ? numberOfPoints - 1 : 0;
            }
            else
            {
                clusterCount = numberOfClusters;
            }

            var centers = new PointF[clusterCount];
            if (!lines.Any())
            {
                return centers;
            }

            ////Код, отвечающий за использование метода Kmeans
            var samples = new Matrix<Single>(numberOfPoints, 2);
            var labels = new Matrix<Int32>(numberOfPoints, 1);
            var centersMatrix = new Matrix<Single>(clusterCount, 2);
            for (Int32 u = 0, k = 0; k < lines.Count(); u += 2, k++)
            {
                samples[u, 0] = lines[k].P1.X;
                samples[u, 1] = lines[k].P1.Y;
                samples[u + 1, 0] = lines[k].P2.X;
                samples[u + 1, 1] = lines[k].P2.Y;
            }

            CvInvoke.cvKMeans2(
                samples,
                clusterCount,
                labels,
                new MCvTermCriteria(),
                2,
                IntPtr.Zero,
                0,
                centersMatrix,
                IntPtr.Zero);

            var labels4Lines = new Matrix<Int32>(lines.Count, 1);
            for (Int32 k = 0; k < lines.Count(); k++)
            {
                labels4Lines[k, 0] = labels[k * 2, 0];
            }

            for (Int32 k = 0; k < clusterCount; k++)
            {
                centers[k].X = centersMatrix[k, 0];
                centers[k].Y = centersMatrix[k, 1];
                var linesgroup = new List<LineSegment2D>();
                for (Int32 i = 0; i < labels4Lines.Rows; i++)
                {
                    if (labels4Lines[i, 0] == k)
                    {
                        linesgroup.Add(lines[i]);
                    }
                }

                if (linesgroup.Count > 1)
                {
                    // find a, b
                    // x1 * a + b = y1
                    // x2 * a + b = y2                   
                    var a1 = new Matrix<Single>(2, 2);
                    var b1 = new Matrix<Single>(2, 1);
                    a1[0, 0] = linesgroup[0].P1.X;
                    a1[0, 1] = 1;
                    a1[1, 0] = linesgroup[0].P2.X;
                    a1[1, 1] = 1;

                    b1[0, 0] = linesgroup[0].P1.Y;
                    b1[1, 0] = linesgroup[0].P2.Y;

                    var x1 = new Matrix<Single>(2, 1);
                    CvInvoke.cvSolve(a1, b1, x1, SOLVE_METHOD.CV_LU);

                    // find a, b
                    // x1 * a + b = y1
                    // x2 * a + b = y2                   
                    var a2 = new Matrix<Single>(2, 2);
                    var b2 = new Matrix<Single>(2, 1);
                    a2[0, 0] = linesgroup[1].P1.X;
                    a2[0, 1] = 1;
                    a2[1, 0] = linesgroup[1].P2.X;
                    a2[1, 1] = 1;

                    b2[0, 0] = linesgroup[1].P1.Y;
                    b2[1, 0] = linesgroup[1].P2.Y;

                    var x2 = new Matrix<Single>(2, 1);
                    CvInvoke.cvSolve(a2, b2, x2, SOLVE_METHOD.CV_LU);

                    // find point
                    // y - a1 * x = b1
                    // y - a2 * x = b2
                    var a3 = new Matrix<Single>(2, 2);
                    var b3 = new Matrix<Single>(2, 1);
                    a3[0, 0] = 1;
                    a3[0, 1] = -x1[0, 0]; // a1
                    a3[1, 0] = 1;
                    a3[1, 1] = -x2[0, 0]; // a2

                    b3[0, 0] = x1[1, 0]; // b1
                    b3[1, 0] = x2[1, 0]; // b2

                    var center = new Matrix<Single>(2, 1);
                    CvInvoke.cvSolve(a3, b3, center, SOLVE_METHOD.CV_LU);

                    centers[k].X = center[1, 0]; // x
                    centers[k].Y = center[0, 0]; // y
                }
            }

            return centers;
        }
    }
}
