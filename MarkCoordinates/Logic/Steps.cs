﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;

namespace MarkCoordinates.Logic
{
    internal class Steps
    {
        private int width, height;
        public Steps(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public void VisualizeEveryStep(MyMatrix myMat, int stepNum, ImageBox imageBox)
        {
            var m = myMat;
            var points = new List<Point>();
            switch (stepNum)
            {
                case 0:
                    points = Step1(m);
                    break;
                case 1:
                    points = Step2(m, Step1(m));
                    break;
                case 2:
                    points = Step3(m, Step2(m, Step1(m)));
                    break;
                case 3:
                    points = Step4(m, Step3(m, Step2(m, Step1(m))));
                    break;
                case 4:
                    points = Step5(m, Step4(m, Step3(m, Step2(m, Step1(m)))));
                    break;
            }
            int minX = 0;
            int minY = 0;
            foreach (var point in points)
            {
                if (point.X < minX) minX = point.X;
                if (point.Y < minY) minY = point.Y;
            }
            Single radius = 8.0f;
            Int32 thickness = -1;

            var image = new Image<Bgr, byte>(width-minX+10, height-minY+10);
            image._Not();
            var pointF = new PointF(points[0].X - minX + 10, points[0].Y - minY + 10);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Red), thickness);
            pointF = new PointF(points[1].X - minX + 10, points[1].Y - minY + 10);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Green), thickness);
            pointF = new PointF(points[2].X - minX + 10, points[2].Y - minY + 10);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Blue), thickness);

            var internalpoints = m.internalPoints.Points2;
            pointF = new PointF(internalpoints[0].X - minX + 10, internalpoints[0].Y - minY + 10);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Orange), thickness);
            pointF = new PointF(internalpoints[1].X - minX + 10, internalpoints[1].Y - minY + 10);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.GreenYellow), thickness);
            pointF = new PointF(internalpoints[2].X - minX + 10, internalpoints[2].Y - minY + 10);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.DeepSkyBlue), thickness);

            var lines = new List<LineSegment2D>();
            lines.Add(new LineSegment2D(new Point(0, -minY + 10), new Point(image.Width - 1, -minY + 10)));
            lines.Add(new LineSegment2D(new Point(-minX + 10, 0), new Point(-minX + 10, image.Height - 1)));
            image.DrawLines(lines, 5);

            imageBox.Image = image;
        }

        private List<Point> Step1(MyMatrix m)
        {
            var R = m.rotateMatrix;
            var internalpoints = m.internalPoints.Points1;
            var resultPoints = new List<Point>();
            foreach (var point in internalpoints)
            {
                var x = (int)(R[0, 0] * point.X + R[0, 1] * point.Y);
                var y = (int)(R[1, 0] * point.X + R[1, 1] * point.Y);
                resultPoints.Add(new Point(x,y));
            }
            return resultPoints;
        }

        private List<Point> Step2(MyMatrix m, List<Point> points)
        {
            var o1 = m.leftOrthogonalMatrix;
            var def = m.deformationMatrix;

            var o1Inv = new Matrix<Single>(2, 2);
            CvInvoke.cvInvert(o1, o1Inv, SOLVE_METHOD.CV_LU);

           

            var resultPoints = new List<Point>();
            foreach (var point in points)
            {
                var x = (int)(o1Inv[0, 0] * point.X + o1Inv[0, 1] * point.Y);
                var y = (int)(o1Inv[1, 0] * point.X + o1Inv[1, 1] * point.Y);

                resultPoints.Add(new Point(x, y));
            }
            return resultPoints;
        }

        private List<Point> Step3(MyMatrix m, List<Point> points)
        {
            var def = m.deformationMatrix;

            var resultPoints = new List<Point>();
            foreach (var point in points)
            {
                var x = (int)(def[0, 0] * point.X + def[0, 1] * point.Y);
                var y = (int)(def[1, 0] * point.X + def[1, 1] * point.Y);

                resultPoints.Add(new Point(x, y));
            }
            return resultPoints;
        }

        private List<Point> Step4(MyMatrix m, List<Point> points)
        {
            var o1 = m.leftOrthogonalMatrix;

            var resultPoints = new List<Point>();
            foreach (var point in points)
            {
                var x = (int)(o1[0, 0] * point.X + o1[0, 1] * point.Y);
                var y = (int)(o1[1, 0] * point.X + o1[1, 1] * point.Y);

                resultPoints.Add(new Point(x, y));
            }
            return resultPoints;
        }

        private List<Point> Step5(MyMatrix m, List<Point> points)
        {
            int Tx = (int)m.translationVector[0, 0];
            int Ty = (int)m.translationVector[1, 0];

            var resultPoints = new List<Point>();
            foreach (var point in points)
            {
                var x = Tx + point.X;
                var y = Ty + point.Y;

                resultPoints.Add(new Point(x, y));
            }
            return resultPoints;
        }
    }
}