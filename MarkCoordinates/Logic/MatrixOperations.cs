﻿namespace MarkCoordinates.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Text;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Windows.Forms.VisualStyles;

    using Emgu.CV;
    using Emgu.CV.CvEnum;
    using Emgu.CV.Structure;    

    // Добавил текстбокс, можно выводить информацию о матрицах, сделал конвертацию списка точек в матрицу
    // Сделал сортировку по расстоянию до центра
    // TODO: Провести разложения матриц, выводить промежуточные результаты   

    internal class Points
    {
        private Point figureCenter1;

        private Point figureCenter2;

        public List<Point> Points1 { get; private set; }

        public List<Point> Points2 { get; private set; }

        public Points(List<Point> points1, List<Point> points2)
        {
            this.Points1 = points1;
            this.Points2 = points2;
            
            figureCenter1 = this.GetCentralPoint(points1);
            figureCenter2 = this.GetCentralPoint(points2);
        }

        private Point GetCentralPoint(List<Point> points)
        {
            int sumX, sumY;
            sumX = sumY = 0;

            for (int i = 0; i < points.Count; i++)
            {
                sumX += points[i].X;
                sumY += points[i].Y;
            }

            int midX, midY;
            midX = sumX / points.Count;
            midY = sumY / points.Count;

            return new Point(midX, midY);
        }

        public Point GetCenterPoint1()
        {
            return figureCenter1;
        }

        public Point GetCenterPoint2()
        {
            return figureCenter2;
        }
    }
    
    //  если прямо не указано, то значение скорее всего хранится в радианах
    internal class Angle
    {
        private double angleValue;
        private double offset;

        public Double AngleValue
        {
            get
            {
                return this.angleValue;
            }
            private set
            {
                this.angleValue = value;
            }
        }

        private Double angleCos;

        private Double angleSin;

        public Angle(Matrix<Single> rotateMatrix)
        {
            this.angleCos = (rotateMatrix[0, 0] + rotateMatrix[1, 1]) / 2;
            this.angleSin = (rotateMatrix[1, 0] - rotateMatrix[0, 1]) / 2;
            var angleVal1 = Math.Acos(Math.Abs(this.angleCos));
            var angleVal2 = Math.Asin(Math.Abs(this.angleSin));
            this.offset = this.Offset(this.angleCos, this.angleSin);

            if ((int)angleVal1 != (int)angleVal2) throw new Exception("Разные значения углов");
            this.AngleValue = angleVal1;                
        }
        //  вычисляем смещение (в градусах, а не в радианах!)
        private Double Offset(Double cos, Double sin)
        {
            if (Math.Sign(cos) == 1 && Math.Sign(sin) == 1)
            {
                return 0;
            }
            if (Math.Sign(cos) == -1 && Math.Sign(sin) == 1)
            {
                return 90;
            }
            if (Math.Sign(cos) == -1 && Math.Sign(sin) == -1)
            {
                return -180; // или 180
            }
            if (Math.Sign(cos) == 1 && Math.Sign(sin) == -1)
            {
                return -90; //  или 270
            }
            return -1;
        }

        public Double GetDegrees()
        {
            var angleInFirstQuarter = this.angleValue * 180 / Math.PI;

            if (Math.Sign(angleCos) == 1 && Math.Sign(angleSin) == 1)
            {
                return angleInFirstQuarter; // I четверть
            }
            if (Math.Sign(angleCos) == -1 && Math.Sign(angleSin) == 1)
            {
                return 180 - angleInFirstQuarter; // II четверть
            }
            if (Math.Sign(angleCos) == -1 && Math.Sign(angleSin) == -1)
            {
                return -180 + angleInFirstQuarter;// III четверть
            }
            if (Math.Sign(angleCos) == 1 && Math.Sign(angleSin) == -1)
            {
                return -angleInFirstQuarter;  //  IV четверть
            }
            return angleInFirstQuarter;
        }
    }

    internal class MyMatrix
    {
        public Points internalPoints;

        public Matrix<Single> deformationMatrix; //diagonalMatrix

        public Matrix<Single> deformationTensor;

        public Matrix<Single> deformationDiff; 

        public Matrix<Single> initConfigMatrix; //initial configuration of points 
        /*
         * Format:
         * x1   x2  x3
         * y1   y2  y3
         * 1    1   1    
         * 
         */

        public Matrix<Single> endConfigMatrix; //end configuration of points

        public Matrix<Single> transformMatrix; //consists of translationVector and centroAffineConversionMatrix 

        public Matrix<Single> translationVector;

        public Matrix<Single> centroAffineConversionMatrix; 

        public Matrix<Single> rotateMatrix;

        public Matrix<Single> leftOrthogonalMatrix;

        public Matrix<Single> rightOrthogonalMatrix;

        public Angle angle;

        public bool isRotationDetNegative;
    }

    /// <summary>
    /// X' = T * X;
    /// T = O_1 * D * O_2 = (O_1 * D * O_1^-1) * (O_1 * O_2)
    /// D - диагональная матрица, элементы которой - коэффициенты растяжения
    /// R = O_1 * O_2 - матрица поворота
    /// </summary>
    internal class MatrixOperations
    {
        private RichTextBox infoTextBox;

        private StringBuilder text;

        MyMatrix myMat = new MyMatrix();


        public MatrixOperations()
        {

        }

        public MatrixOperations(Points points, RichTextBox infoTextBox)
        {
            myMat.internalPoints = points;
            myMat.initConfigMatrix = this.Convert2Matrix(points.Points1);
            myMat.endConfigMatrix = this.Convert2Matrix(points.Points2);
            this.infoTextBox = infoTextBox;
            text = new StringBuilder();
        }
        
        public MatrixOperations(Points points)
        {
            myMat.internalPoints = points;
            myMat.initConfigMatrix = this.Convert2Matrix(points.Points1);
            myMat.endConfigMatrix = this.Convert2Matrix(points.Points2);
        }

        internal void PlainWorkflow()
        {
            myMat.transformMatrix = this.GetTransformMatrix(myMat.initConfigMatrix, myMat.endConfigMatrix);
            this.GetPartsOfTransformMatrix(myMat.transformMatrix);
            this.MakeSVD(myMat.centroAffineConversionMatrix);
            //теперь deformationMatrix = new Matrix<Single>(2, 2); доступна
        }

        internal void UltimateWorkflow()
        {
            ////text.AppendFormat("Init matrix is \n{0}",this.Matrix2String(initConfigMatrix));
            this.Log(myMat.initConfigMatrix, "Init matrix");
            this.Log(myMat.endConfigMatrix, "End matrix");

            myMat.transformMatrix = this.GetTransformMatrix(myMat.initConfigMatrix, myMat.endConfigMatrix);
            GetPartsOfTransformMatrix(myMat.transformMatrix);
            this.MakeSVD(myMat.centroAffineConversionMatrix);
            this.MakeRotateMatrix();

            Double det = CvInvoke.cvDet(myMat.rotateMatrix);
            if (det < 0)
            {
                myMat.isRotationDetNegative = true;
                throw new Exception("Negative determinant");
            }
            else
            {
                myMat.isRotationDetNegative = false;
            }

            myMat.angle = new Angle(myMat.rotateMatrix);
            this.MakeDeformationTensor();
            MakeDeformationDiff();
            ////var degrees = myMat.angle.GetDegrees();
            ////text.AppendFormat("\n RotateAngle = {0}", degrees);

            ////this.ShowInfo(text.ToString());

        }

        private Matrix<Single> GetTransformMatrix(Matrix<Single> init, Matrix<Single> end)
        {
            var invInit = new Matrix<Single>(3, 3);
            CvInvoke.cvInvert(init, invInit, SOLVE_METHOD.CV_LU);
            var transform = end * invInit;

            //text.AppendFormat("Inverse init matrix is \n{0}", this.Matrix2String(invInit));
            this.Log(transform, "Transform matrix");
            
            //  Проверяем матрицу трансформации, resultcheck должен быть похож на end
            var resultcheck = transform * init; 
            this.Log(resultcheck, "Check result");
            return transform;
        }

        private void GetPartsOfTransformMatrix(Matrix<Single> transform)
        {
            myMat.translationVector = transform.GetCol(2).GetRows(0, 2, 1);
            this.Log(myMat.translationVector, "translationVector");

            myMat.centroAffineConversionMatrix = transform.GetCols(0, 2).GetRows(0, 2, 1);
            this.Log(myMat.centroAffineConversionMatrix, "centroAffineConverionMatrix");
        }

        //TODO проверить что разложение происходит правильно - перемножить финальные матрицы, должна получиться матрица аффинного преобразования

        private void MakeSVD(Matrix<Single> affineConversionMat)
        {
            myMat.leftOrthogonalMatrix = new Matrix<Single>(2, 2);
            myMat.rightOrthogonalMatrix = new Matrix<Single>(2, 2);
            myMat.deformationMatrix = new Matrix<Single>(2, 2);

            CvInvoke.cvSVD(affineConversionMat, myMat.deformationMatrix, myMat.leftOrthogonalMatrix, myMat.rightOrthogonalMatrix, SVD_TYPE.CV_SVD_V_T);

            //  следующие действия по смене знака не влияют на исход svd разложения
            //  похоже, что svd разложение может проводится разными способами. И способ в OpenCV отличается от способа разложения в mathcad функции svd2.
            //  отличается знаками второго столбца левой матрицы и второй строчки правой матрицы
            //  произведения от этого не меняются, т.е. leftOrthogonalMatrix * deformationMatrix * rightOrthogonalMatrix будет по-прежнему давать affineConversionMat
            //  но проблема в том, что из-за знаков такого разложения, при умножении на них координат, оси инвертируются (речь о произведении O1*D*01^-1). 
            //  В целом на результат это не влияет, но если визуализировать каждый шаг (умножение O1^-1 на вектор X, затем умножение D на вектор X и, наконец, умножение  O1 на X), 
            //  то получится что при домножении на O1^-1 у нас координаты станут отрицательными, потом мы производим растяжение, 
            //  домножаем на обратную матрицу и возвращаемся к положительным координатам
            //  если знаки поменять сразу, то работа будет происходить именно в положительных осях

            //   похоже, подход OpenCV тоже имеет свои основания. Если домножать как ниже (и в mathcad'e). То фигура инвертируется. Зато она в положительных осях
              
            //myMat.leftOrthogonalMatrix[0, 1] *= -1;
            //myMat.leftOrthogonalMatrix[1, 1] *= -1;

            //myMat.rightOrthogonalMatrix[1, 0] *= -1;
            //myMat.rightOrthogonalMatrix[1, 1] *= -1;

            var matrix = myMat.leftOrthogonalMatrix * myMat.deformationMatrix * myMat.rightOrthogonalMatrix;

            ////this.Log(affineConversionMat, "Before");
            ////this.Log(leftOrthogonalMatrix *  deformationMatrix * rightOrthogonalMatrix, "After");
        }
        private void MakeRotateMatrix()
        {

            //Если определитель матрицы поворота равен -1 - это значит, что произошла инверсия системы, и что-то пошло не так
            myMat.rotateMatrix = myMat.leftOrthogonalMatrix * myMat.rightOrthogonalMatrix; 

            this.Log(myMat.rotateMatrix, "RotateMatrix");
            if (text != null)
            {
                text.AppendFormat("Determinant = {0}", myMat.rotateMatrix.Det);
                text.AppendFormat(
                    "\nDeterminant = {0}\n",
                    myMat.rotateMatrix[0, 0] * myMat.rotateMatrix[1, 1] - myMat.rotateMatrix[1, 0] * myMat.rotateMatrix[0, 1]);
            }

            var test = myMat.rotateMatrix * myMat.rotateMatrix.Transpose();
            this.Log(test, "identity matrix ?");

        }

        private void MakeDeformationTensor()
        {
            var ones = new Matrix<Single>(2,2);
            ones.SetIdentity();
            myMat.deformationTensor = myMat.centroAffineConversionMatrix - ones;
        }

        /// <summary>
        /// строим вспомогательную матрицу разницы элементов матрицы деформации и тензора деформации
        /// </summary>
        private void MakeDeformationDiff()
        {
            myMat.deformationDiff = new Matrix<Single>(2, 2);
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                {
                    myMat.deformationDiff[i, j] = myMat.deformationMatrix[i, j] - myMat.deformationTensor[i, j];
                }
        }

        private void ShowInfo(string info)
        {
            //for (int i = 0; i < 3; i++)
            //{
            //    initConfigMatrix[i, 0] = i + 1;
            //    initConfigMatrix[i, 1] = i + 2;
            //    initConfigMatrix[i, 2] = i + 3;                   
            //}           
            if (infoTextBox != null)
            {
                infoTextBox.Text = info; 
            }
        }

        private StringBuilder Matrix2String(Matrix<Single> matrix)
        {
            var text = new StringBuilder();
            text.AppendLine(String.Format("Width = {0}, Height = {1}\n", matrix.Width, matrix.Height));
            for (int i = 0; i < matrix.Height; i++)
            {
                for (int j = 0; j < matrix.Width; j++)
                {
                    text.AppendFormat("{0}\t", matrix[i, j]);
                }
                text.AppendLine();
            }
            return text;
        }

        private void Log(Matrix<Single> mat, string name)
        {
            if (text != null)
            {
                text.AppendFormat("{0} is\n {1}", name, this.Matrix2String(mat));
            }
        }

        /// <summary>
        /// Convert List of points to matrix (3,3) 
        /// </summary>
        /// <remarks>
        /// x1  x2  x3
        /// y1  y2  y3
        /// 1   1   1
        /// </remarks>
        public Matrix<Single> Convert2Matrix(List<Point> points)
        {
            var mat = new Matrix<Single>(3, 3);
            for (int i = 0; i < 3; i++)
            {
                mat[0, i] = points[i].X;
                mat[1, i] = points[i].Y;
                mat[2, i] = 1;
            }
            return mat;
        }

        public Matrix<Single> GetDeformationMatrix()
        {
            if (myMat.deformationMatrix != null)
            {
                return myMat.deformationMatrix;
            }
            else
            {
                throw new Exception("пустая матрица деформации");
            }
        }

        public Matrix<Single> GetDeformationTensor()
        {
            return myMat.deformationTensor;
        }
        public List<Single> GetDeformationCoefficients()
        {
            var coefficients = new List<Single>();
            if (myMat.deformationMatrix != null)
            {
                coefficients.Add(myMat.deformationMatrix[0, 0]);
                coefficients.Add(myMat.deformationMatrix[1, 1]);
            }
            return coefficients;
        }

        public MyMatrix GetMyMatrix()
        {
            return myMat;
        }

        public void UltimateTest()
        {
            //  запускать после UltimateWorkflow
            var coefmatrix = myMat.leftOrthogonalMatrix * myMat.deformationMatrix * myMat.rightOrthogonalMatrix;
            //myMat.translationVector
        }
    }
}