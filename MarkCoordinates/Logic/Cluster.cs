﻿namespace MarkCoordinates.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Emgu.CV;
    using Emgu.CV.Structure;
    using Emgu.Util;
    
    /// <summary>
    /// Container for points data.
    /// </summary>
    public class Cluster
    {
        private List<PointF> points;
        private Single xsum;
        private Single ysum;
        private PointF center;

        public Cluster()
        {
            this.xsum = 0;
            this.ysum = 0;
            //default center coordinates (if it will be like  this, we know that mistake occurs)
            this.center.X = -1;
            this.center.Y = -1;
            points = new List<PointF>();
        }

        /// <summary>
        /// Center of cluster.
        /// </summary>
        public PointF Center
        {
            get
            {
                if (!points.Any())
                {
                    return center;
                }

                center.X = xsum / points.Count();
                center.Y = ysum / points.Count();
                return center;
            }
        }

        /// <summary>
        /// Number of points in cluster.
        /// </summary>
        public Int32 Count 
        {
            get
            {
               return points.Count();                
            }
        }

        /// <summary>
        /// Adding point to internal List of PointF.
        /// </summary>
        /// <param name="point">New point.</param>
        public void AddPointF(PointF point)
        {
            points.Add(point);
            xsum += point.X;
            ysum += point.Y;
        }
    }
}
