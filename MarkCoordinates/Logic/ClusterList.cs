﻿namespace MarkCoordinates.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
   
    using Emgu.CV;
    using Emgu.CV.CvEnum;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class for extracting sets of nearest points (clusters) from image.
    /// </summary>
    /// <param name="centerPointsF">Все внутренние операции производятся с float-центром, если хотим получить те же координаты в int - то смотрим как реализовано свойство IntCenterPoints</param>
    class ClusterList
    {
        /// <summary>
        /// all found contours we put here
        /// </summary>
        private List<Contour<Point>> _allContours;

        /// <summary>
        /// centers of contours from allContours
        /// </summary>
        private List<PointF> _centerPointsF;

        /// <summary>
        /// lines that connect center points
        /// </summary>
        private List<LineSegment2D> _lineSegments;

        /// <summary>
        /// number of columns when we sort
        /// </summary>
        private int _columns;

        /// <summary>
        /// variable of scale - pixels (Height of rectangle) to cm ratio
        /// </summary>
        private Single _scale;

        /// <summary>
        /// Четырехугольник, от которого масштаб будем считать
        /// </summary>
        private Rectangle _rect;

        private readonly Image<Gray, byte> _binaryImage;

        //  binaryImage - input image, from which we will exclude clusters
        //  class should work only in this form.
        public ClusterList(Image<Gray, Byte> binaryImage) 
        {
            this._allContours = new List<Contour<Point>>();
            ////this.centerPoints = new List<CenterPoints>();
            this._centerPointsF = new List<PointF>();
            this._lineSegments = new List<LineSegment2D>();
            //  по умолчанию высота rect = 1cm
            this._scale = 1;
            this._binaryImage = binaryImage;
        }

        #region public properties
        /// <summary>
        /// all found contours we put here
        /// </summary>
        public List<Contour<Point>> AllContours
        {
            get
            {
                return this._allContours;
            }
        }

        /// <summary>
        /// centers of contours from allContours
        /// </summary>
        ///         
        public List<PointF> CenterPointsF
        {
            get
            {
                return this._centerPointsF;
            }
        }
     
        /// <summary>
        /// Используется метод PointFToPoint для конвертации
        /// </summary>
        public List<Point> IntCenterPoints
        {
            get
            {
                List<Point> intCenterPoints = _centerPointsF.ConvertAll(PointFToPoint);
                return intCenterPoints;
            }
        }
        public static Point PointFToPoint(PointF pf)
        {
            return new Point(((int)pf.X), ((int)pf.Y));
        }


        /// <summary>
        /// variable of scale - pixels (Height of rectangle) to cm ratio
        /// </summary>
        public float Scale
        {
            get
            {
                return this._scale;
            }
        }

        /// <summary>
        /// Четырехугольник, от которого масштаб будем считать
        /// </summary>
        public Rectangle Rect
        {
            get
            {
                return this._rect;
            }
        }

        /// <summary>
        /// lines that connect center points
        /// </summary>
        public List<LineSegment2D> LineSegments
        {
            get
            {
                return this._lineSegments;
            }
        }

        #endregion

        public void UltimateWorkflow()
        {
            this.ExtractAllContours();
            
            //  используется хак
            //  предполагается, что контуров должно быть только 4 (3 круга и 1 квадрат)
            //  мы упорядочиваем их по площади, а следовательно последней фигурой (в конкретном данном случае) будет квадрат
            //  его размеры в пикселях мы и берем, чтобы вычислить отношение.
            //  но это частный случай, а потому этот способ нуждается в рефакторинге
            this._allContours = OrderByArea(this.AllContours);

            this._rect = SetRect(this.AllContours.Last());
            //  1.0f - 
            this.Pixel2Cm(1.0f, this.Rect);
            // функцию поиска центров нужно выполнять после сортировки, либо объединить центральную точку и список всех точек в один объект
            // чтобы они вместе сортировались (т.е. создать вспомогательный класс вместо CenterPoints)
            this.GetCenters();

            _columns = 4;
            _centerPointsF = OrderByPositionFromTopLeft(_centerPointsF, _columns);
            ////this.GetLines(3);          
        }

        /// <summary>
        /// Fill our allContours list here and Draw each contour on the internal Image
        /// uses internal field - binaryImage
        /// </summary>        
        /// <remarks>Probably need to remove MemStorage. Don't actually know why we need it.</remarks>
        public void ExtractAllContours(int? minPixelsThreshold)
        {
            using (var storage = new MemStorage()) //allocate storage for contour approximation
            {
                Contour<Point> contours;
                for (contours = _binaryImage.FindContours(
                    Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE,
                    Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST);
                    contours != null;
                    contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(0.001, storage);
                    //  следующий код для указания типов площадей, которые включаем
                    int imageArea = _binaryImage.Height * _binaryImage.Width;

                    //   impose conditions on the size of the region
                    //  отсекаем как слишком большие так и мелкие (во втром случае просто количество пикселей считаем) - опытный параметр
                    if (contours.Area < 0.8 * imageArea && contours.Area > (minPixelsThreshold ?? 100))
                    {
                        _allContours.Add(contours);
                    }                 
                }
            }
        }

        public void ExtractAllContours()
        {
            this.ExtractAllContours(null);
        }

        /// <summary>
        /// Ordering contours in our allContours by Area from up to down. 
        /// </summary>
        /// <param name="contours">Inner contours list.</param>
        /// <returns>Rewrite inner contour list.</returns>
        private List<Contour<Point>> OrderByArea(IEnumerable<Contour<Point>> contours)
        {
            return contours.OrderByDescending(contour => contour.Area).ToList();
        }

        /// <summary>
        /// Бывший метод SortPoints из Workflow класса
        /// </summary>
        /// <param name="points"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static List<PointF> OrderByPositionFromTopLeft(IEnumerable<PointF> points, int columns)
        {
            //  сначала сортируем по Y координате, затем группируем по делению на количество столбцов, которые хотим получить (скажем, 5 столбцов)
            //  далее для каждой группы (допустим в итоге у нас три группы) мы производим свою сортировку по X координате
            //  в итоге sortedPoints содержит отсортированные сначала по Y, а затем по X элементы
            var helpPoints = points.OrderBy(p => p.Y)
                                        .Select((value, index) => new { index, value })
                                        .GroupBy(element => element.index / columns)
                                        .ToList();
            var sortedPoints = new List<PointF>();
            foreach (var group in helpPoints)
            {
                var sortedGroup = group.OrderBy(element => element.value.X).ToList();

                foreach (var element in sortedGroup)
                {
                    sortedPoints.Add(element.value);
                }
            }
            return sortedPoints;
        }

        /// <summary>
        /// Need filled inner list of Contours before executing.
        /// Fill inner list centerPoints.
        /// </summary>
        /// <remarks>know inner list centerPoints contains int values, but we can make it float(better accuracy).</remarks>
        private void GetCenters()
        {

            foreach (var contour in this.AllContours)
            {
                var moments = contour.GetMoments();
                double x = moments.m10 / moments.m00;
                double y = moments.m01 / moments.m00;
                var centerPointF = new PointF((float)x, (float)y);            
                _centerPointsF.Add(centerPointF);
            }
        }

        /// <summary>
        /// Lines between center points.
        /// </summary>
        /// <param name="connectSpecific">If we want to connect not all points.</param>
        private void GetLines(int connectSpecific)
        {
            int n = connectSpecific > this.IntCenterPoints.Count() ? this.IntCenterPoints.Count() : connectSpecific;
            if (n > 0)
            {
                for (int i = 0; i < n - 1; i++)
                {
                    this.LineSegments.Add(new LineSegment2D(this.IntCenterPoints[i], this.IntCenterPoints[i + 1]));
                }
                this.LineSegments.Add(new LineSegment2D(this.IntCenterPoints[n - 1], this.IntCenterPoints[0]));
            }
        }

        // TODO вынести калибровочные методы в отдельный класс
        private Rectangle SetRect(Contour<Point> contour)
        {
            //  получив контур квадрата, мы берем охватывающий его прямоугольник
            return contour.BoundingRectangle;
        }

        /// <summary>
        /// Метод калибровки
        /// </summary>
        /// <param name="cm">Сколько сантиметров - длина грани прямоугольника</param>
        /// <param name="rect">Сам прямоугольник</param>
        private void Pixel2Cm(float cm, Rectangle rect)
        {
            this._scale = cm / rect.Height;
        }
    }
}
