﻿namespace MarkCoordinates.Logic
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using Excel = Microsoft.Office.Interop.Excel; 

    class MatrixDataTable
    {
        DataTable dt;

        public MatrixDataTable()
        {
            InitDT();
        }
        private void InitDT()
        {
            dt = new DataTable();
            dt.Columns.Add("PId");
            dt.Columns.Add("isRotationDetNegative");
            dt.Columns.Add("Xc1");
            dt.Columns.Add("Yc1");
            dt.Columns.Add("Xc2");
            dt.Columns.Add("Yc2");
            dt.Columns.Add("Angle");
            dt.Columns.Add("TransX");
            dt.Columns.Add("TransY");
            dt.Columns.Add("Def1");
            dt.Columns.Add("Def2");
            dt.Columns.Add("DefAxeX1");
            dt.Columns.Add("DefAxeX2");
            dt.Columns.Add("DefAxeY1");
            dt.Columns.Add("DefAxeY2");
            dt.Columns.Add("p1X");
            dt.Columns.Add("p1Y");
            dt.Columns.Add("p2X");
            dt.Columns.Add("p2Y");
            dt.Columns.Add("p3X");
            dt.Columns.Add("p3Y");
            dt.Columns.Add("p1Xnext");
            dt.Columns.Add("p1Ynext");
            dt.Columns.Add("p2Xnext");
            dt.Columns.Add("p2Ynext");
            dt.Columns.Add("p3Xnext");
            dt.Columns.Add("p3Ynext");
        }

        public void AddRow(
            int Xc1,        //  координаты центра фигуры на первом кадре
            int Yc1,
            int Xc2,        //  координаты центра фигуры на втором кадре
            int Yc2,
            Double angle,   //  угол поворота центра
            int Tx,         //  смещение центра фигуры (Translation)
            int Ty,
            Single d1,      //  растяжение-сжатие по главной оси деформации W1
            Single d2,      //  растяжение-сжатие по главной оси деформации W2
            Single W1x,     //  координаты единичного орта главной оси деформации W1 в базисной системе координат
            Single W1y,
            Single W2x,     //  координаты единичного орта главной оси деформации W2 в базисной системе координат
            Single W2y,
            int? p1X = null,
            int? p1Y = null,
            int? p2X = null,
            int? p2Y = null,
            int? p3X = null,
            int? p3Y = null,
            int? p1Xnext = null,
            int? p1Ynext = null,
            int? p2Xnext = null,
            int? p2Ynext = null,
            int? p3Xnext = null,
            int? p3Ynext = null,
            bool isRotDetNeg = false)
        {
            DataRow row = dt.NewRow();
            if (dt.Rows.Count == 0)
            {
                row["PId"] = 1;
            }
            else
            {
                string pid = (string)dt.Rows[dt.Rows.Count - 1]["PId"];
                row["PId"] = Convert.ToInt32(pid) + 1;
            }
            row["isRotationDetNegative"] = isRotDetNeg;
            row["Xc1"] = Xc1;
            row["Yc1"] = Yc1;
            row["Xc2"] = Xc2;
            row["Yc2"] = Yc2;
            row["Angle"] = angle;
            row["TransX"] = Tx;
            row["TransY"] = Ty;
            row["Def1"] = d1;
            row["Def2"] = d2;
            row["DefAxeX1"] = W1x;
            row["DefAxeX2"] = W1y;
            row["DefAxeY1"] = W2x;
            row["DefAxeY2"] = W2y;
            row["p1X"] = p1X;
            row["p1Y"] = p1Y;
            row["p2X"] = p2X;
            row["p2Y"] = p2Y;
            row["p3X"] = p3X;
            row["p3Y"] = p3Y;
            row["p1Xnext"] = p1Xnext;
            row["p1Ynext"] = p1Ynext;
            row["p2Xnext"] = p2Xnext;
            row["p2Ynext"] = p2Ynext;
            row["p3Xnext"] = p3Xnext;
            row["p3Ynext"] = p3Ynext;

            dt.Rows.Add(row);
        }

        public void AddRow(MyMatrix m)
        {
            var point1center = m.internalPoints.GetCenterPoint1();
            var point2center = m.internalPoints.GetCenterPoint2();
            double angle = m.angle.GetDegrees();
            int Tx = (int)m.translationVector[0, 0];
            int Ty = (int)m.translationVector[1, 0];

            int? p1X = m.internalPoints.Points1[0].X,
                 p1Y = m.internalPoints.Points1[0].Y,
                 p2X = m.internalPoints.Points1[1].X,
                 p2Y = m.internalPoints.Points1[1].Y,
                 p3X = m.internalPoints.Points1[2].X,
                 p3Y = m.internalPoints.Points1[2].Y,
                 p1Xnext = m.internalPoints.Points2[0].X,
                 p1Ynext = m.internalPoints.Points2[0].Y,
                 p2Xnext = m.internalPoints.Points2[1].X,
                 p2Ynext = m.internalPoints.Points2[1].Y,
                 p3Xnext = m.internalPoints.Points2[2].X,
                 p3Ynext = m.internalPoints.Points2[2].Y;

            AddRow(point1center.X, point1center.Y, point2center.X, point2center.Y, angle, 
                Tx, Ty, m.deformationMatrix[0,0], m.deformationMatrix[1,1], 
                m.rightOrthogonalMatrix[0,0], m.rightOrthogonalMatrix[0, 1], m.rightOrthogonalMatrix[1,0], m.rightOrthogonalMatrix[1,1]
                ,p1X,p1Y,p2X, p2Y, p3X, p3Y, p1Xnext, p1Ynext, p2Xnext, p2Ynext, p3Xnext, p3Ynext, m.isRotationDetNegative);
        }

        ////private Point GetCenterPoint(List<Point> pList)
        ////{
        ////    int Xc = pList.Sum(p => p.X) / pList.Count();
        ////    int Yc = pList.Sum(p => p.Y) / pList.Count();
        ////    return new Point(Xc, Yc);
        ////}

        public DataTable GetDataTable()
        {
            return dt;
        }
        public void ExportToExcel(DataTable dtTable, string pathFileName)
        {
            try
            {
                var excel = new Excel.Application { Visible = false };
                var misValue = System.Reflection.Missing.Value;
                var wb = excel.Workbooks.Add(misValue);
                ////var wb = excel.Workbooks.Open(pathFileName);
                Excel.Worksheet sh = wb.Sheets.Add();
                sh.Name = "TestSheet";
                int k = 0;
                foreach (DataColumn column in dtTable.Columns)
                {
                    sh.Cells[1, ++k].Value2 = column.ColumnName;
                }
                var columnsCount = dtTable.Columns.Count;
                /* Insert Rows */
                for (int i = 0; i < dtTable.Rows.Count; i++)
                {
                    var row = dtTable.Rows[i];
                    for (int j = 0; j < columnsCount; j++)
                    {
                        sh.Cells[i + 2, j + 1].Value2 = dtTable.Rows[i][j]; 
                    }
                }

                //  по умолчанию сохраняет в C:\Users\username\Documents
                wb.SaveAs(pathFileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                wb.Close(true);
                excel.Quit();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
