﻿namespace MarkCoordinates.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;

    using Emgu.CV;
    using Emgu.CV.CvEnum;
    using Emgu.CV.Structure;

    /// <summary>
    /// Method extensions for Image.
    /// </summary>
    internal static class ImageDrawingExtensions
    {
        // TODO: here we put methods for result's visualization 

        /// <summary>
        /// auxiliary list of colors, which we already used
        /// Сразу добавляем черный цвет к списку использованных цветов
        /// </summary>
        private static readonly List<Color> usedColors = new List<Color> { Color.Black };
        static Random randomGen = new Random();

        private static Color currentColor = Color.Empty;

        public static Color CurrentColor
        {
            get
            {
                return currentColor;
            }
            set
            {
                currentColor = value;
            }
        }

        public static Color RandomColor()
        {
            Color randomColor;
            do
            {
                KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
                KnownColor randomColorName = names[randomGen.Next(names.Length)];
                randomColor = Color.FromKnownColor(randomColorName);
            }
            while (usedColors.Contains(randomColor));
            usedColors.Add(randomColor);
            return randomColor;
        }

        /// <summary>
        /// Generate new Color.
        /// Fill usedColors List so to not repeat them (what can cause ambiguity when interpret result image). 
        /// </summary>
        /// <returns>Bgr color.</returns>
        public static Bgr RandomBgrColor()
        {
            Color randomColor = RandomColor();
            
            return new Bgr(randomColor);
        }


        /// <summary>
        /// Draw multiple contours
        /// </summary>
        /// <param name="outImage"></param>
        /// <param name="allContours"></param>
        public static void DrawContours(this Image<Bgr, Byte> outImage, List<Contour<Point>> allContours)
        {
            
            foreach (var contour in allContours)
            {
                var color = new Bgr(Color.Black);
                outImage.Draw(contour, color, 2);
            }
        }
        
        public static void DrawFilledContours(this Image<Bgr, Byte> outImage, List<Contour<Point>> allContours)
        {
            
            foreach (var contour in allContours)
            {
                var color = RandomBgrColor();
                outImage.Draw(contour, color, -1);
            }
        }



        public static void DrawCenterPoints(this Image<Bgr, Byte> outImage, List<PointF> centerPoints)
        {
            ////int number = 1;
            ////var font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 5.0, 5.0);

            var col = CurrentColor; 
            var color = new Bgr(col != Color.Empty ? col : Color.Black);
            foreach (var centerPoint in centerPoints)
            {
                outImage.Draw(new CircleF(centerPoint, 4.0f), color, 2);
                ////outImage.Draw(number.ToString(CultureInfo.InvariantCulture), ref font, centerPoint.intPoint, RandomColor());
                ////number++;
            }
        }

        public static void DrawCenterPoint(this Image<Bgr, Byte> outImage, PointF centerPointF)
        {
            var col = CurrentColor;
            var color = new Bgr(col != Color.Empty ? col : Color.Black);
            outImage.Draw(new CircleF(centerPointF, 4.0f), color, 2);
        }

        /// <summary>
        /// Draw multiple lines.
        /// </summary>
        /// <param name="outImage">Image, where we will draw.</param>
        /// <param name="lines">List of lines.</param>
        public static void DrawLines(this Image<Bgr, Byte> outImage, List<LineSegment2D> lines, int thickness)
        {
            foreach (var line in lines)
            {
                outImage.Draw(line, new Bgr(Color.Black), thickness);
            }
            ////DrawLinesLength(outImage, lines, scale);
        }

        public static void DrawFigureLines(this Image<Bgr, Byte> outImage, List<Point> figure)
        {
            var lines = new List<LineSegment2D>();
            int n = figure.Count;
            for (int i = 0; i < n - 1; i++)
            {
                lines.Add(new LineSegment2D(figure[i], figure[i + 1]));
            }
            lines.Add(new LineSegment2D(figure[n - 1], figure[0]));

            var color = new Bgr(Color.Black);
            foreach (var line in lines)
            {
                
                outImage.Draw(line, color, 2);
            }
        }
        

        ////private static void DrawLinesLength(Image<Bgr, Byte> outImage, IEnumerable<LineSegment2D> lines, float scale)
        ////{
        ////    var font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
        ////    foreach (var line in lines)
        ////    {
        ////        var lineCenterX = line.P2.X > line.P1.X ? (line.P2.X - line.P1.X) / 2 + line.P1.X
        ////                                                : (line.P1.X - line.P2.X) / 2 + line.P2.X;
        ////        var lineCenterY = line.P2.Y > line.P1.Y ? (line.P2.Y - line.P1.Y) / 2 + line.P1.Y
        ////                                                : (line.P1.Y - line.P2.Y) / 2 + line.P2.Y;

        ////        var point = new Point(lineCenterX, lineCenterY);
        ////        outImage.Draw(Pixels2CmMessage((int)line.Length, scale), ref font, point, new Bgr(Color.Azure));
        ////    }
        ////}

        /////// <summary>
        /////// Draw rectangle
        /////// </summary>
        /////// <param name="outImage"></param>
        /////// <param name="rect"></param>
        /////// <param name="scale"></param>
        ////public static void DrawCalibrationRect(this Image<Bgr, Byte> outImage, Rectangle rect, Single scale)
        ////{
        ////    outImage.Draw(rect, RandomColor(), 1);
        ////    var font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
        ////    var point = new Point(rect.X + rect.Width + 5, rect.Y + rect.Height / 2);
        ////    var message = Pixels2CmMessage(rect.Height, scale);
        ////    outImage.Draw(message, ref font, point,  new Bgr(Color.Azure));
        ////}
        ////private static string Pixels2CmMessage(int pixelsCount, Single scale)
        ////{
        ////    var message = String.Format("{0:0.00} cm", scale * pixelsCount);
        ////    return message;
        ////}
    }
}
