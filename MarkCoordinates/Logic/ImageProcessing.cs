﻿namespace MarkCoordinates.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    
    using Emgu.CV;
    using Emgu.CV.Structure;

    internal static class ImageProcessing
    {
        // TODO: here we processing images in a way we want
        public static Image<Gray, Byte> GetBinaryImage(Image<Bgr, Byte> img)
        {
            const int ThresholdValue = 50;
            var gray = img.Convert<Gray, Byte>();

            return gray.ThresholdBinary(new Gray(ThresholdValue), new Gray(255));
        }

        public static Image<Gray, Byte> CannyFromBgr(Image<Bgr, Byte> img)
        { 
            return CannyFromBgr(img, 180.0, 120.0);
        }

        public static Image<Gray, Byte> CannyFromBgr(Image<Bgr, Byte> img, double cannyThreshold, double cannyThresholdLinking)
        {
            // Convert the image to grayscale
            Image<Gray, Byte> gray = img.Convert<Gray, Byte>();

            // Canny detection
            Image<Gray, Byte> cannyEdges = gray.Canny(cannyThreshold, cannyThresholdLinking);            
            return cannyEdges;
        }

        public static LineSegment2D[] GetLinesFromCanny(Image<Gray, Byte> cannyEdges)
        {
            // use Hough Lines TODO: know more details about it
            return cannyEdges.HoughLinesBinary(
                1, // Distance resolution in pixel-related units
                Math.PI / 45.0, // Angle resolution measured in radians.
                20, // threshold
                30, // min Line width
                10)[0]; // gap between lines; Get the lines from the first channel                
        }
    }
}
