﻿namespace MarkCoordinates
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Linq;
    using System.Resources;
    using System.Threading;
    using System.Web.Services.Description;
    using System.Windows.Forms;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;

    using Emgu.CV;
    using Emgu.CV.CvEnum;
    using Emgu.CV.Structure;
    using Emgu.CV.UI;

    using MarkCoordinates.Logic;

    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            DataBind();
        }

        private void DataBind()
        {
            var myMatrix = new MyMatrix();
            var fields = myMatrix.GetType().GetFields();
            List<String> matrixTypes = new List<string>(); 
            foreach (var fieldInfo in fields)
            {
                if (fieldInfo.FieldType == typeof(Matrix<Single>))
                    matrixTypes.Add(fieldInfo.Name);
            }
            listBox3.DataSource = matrixTypes;
        }

        private int width, height;

        private Image<Gray, Byte> binaryImage;

        private Image<Bgr, Byte> outImage;

        private Image<Gray, Byte> binImage1;
        private Image<Gray, Byte> binImage2;

        private void OpenImageBtn_Click(object sender, EventArgs e)
        {
            //  #HACK Здесь указываем одно изображение 
            var bitmap = new Bitmap(Properties.Resources.synthetic2);
            var orImage = new Image<Bgr, Byte>(bitmap);
            width = bitmap.Width;
            height = bitmap.Height;
            imageBox1.Image = orImage;

            BWButton.Enabled = true;
            BinaryButton.Enabled = true;
        }

        private void BWButton_Click(object sender, EventArgs e)
        {
            var bwImage = new Image<Bgr, Byte>(imageBox1.Image.Bitmap).Convert<Gray, Byte>();
            imageBox1.Image = bwImage;
        }

        private void BinaryButton_Click(object sender, EventArgs e)
        {
            binaryImage = new Image<Gray, Byte>(imageBox1.Image.Bitmap).ThresholdBinary(new Gray(70.0), new Gray(255.0)); 
            imageBox1.Image = binaryImage;

            ClusterButton.Enabled = true;
        }

        private void ClusterButton_Click(object sender, EventArgs e)
        {
            var points = Workflow.GetPointsFromImage(binaryImage, 1, imageBox1, imageBox2);
            var figuresList = this.GetThreePointsFigures(points);
            
            outImage = new Image<Bgr, Byte>(imageBox2.Image.Bitmap);
            foreach (var figure in figuresList)
            {
                outImage.DrawFigureLines(figure);
                ////outImage.DrawCenterPoint(this.FindCenter(figure));
                imageBox2.Image = outImage;
            }

            imageBox2.Image = outImage;
        }

        private List<List<Point>> GetThreePointsFigures(List<Point> points)
        {
            if (points.Count != 20)
            {
                var message = String.Format("точек {0}", points.Count);
                throw new Exception(message);
            }
            // число столбцов (это же значение захардкожено в ClusterList)
            var columns = 4;

            //  разбиваем на треугольники с точками
            var figuresList = new List<List<Point>>();

            var i = 0;
            var lastElem = columns - 1;
            while (columns + i < points.Count)
            {
                // Не учитываем последний элемент в строке
                if (i < lastElem)
                {
                    //  верхние три точки 
                    var figure = new List<Point> { points[i], points[i + 1], points[i + columns] };
                    figuresList.Add(figure);

                    //  нижние три точки 
                    figure = new List<Point> { points[i + 1], points[i + columns], points[i + columns + 1] };
                    figuresList.Add(figure);
                }
                else
                {
                    lastElem += columns;
                }
                i++;
            }
            return figuresList;
        }

        private void UseDefaultButton_Click(object sender, EventArgs e)
        {
            var bitmap = new Bitmap(Properties.Resources.nonhomogrid);
            var orImage = new Image<Bgr, Byte>(bitmap);
            var bwImage = orImage.Convert<Gray, Byte>();
            binaryImage = bwImage.ThresholdBinary(new Gray(127.0), new Gray(255.0));
            //binaryImage = binaryImage.Dilate(1);
            imageBox1.Image = binaryImage;
        }

      
        private void FirstImage(string filepath)
        {
            Bitmap bitmap;
            if (!String.IsNullOrEmpty(filepath))
            {
                bitmap = new Bitmap(filepath);
            }
            else
            {
                bitmap = new Bitmap(Properties.Resources.basis_min);
            }
            var bwImage = new Image<Gray, Byte>(bitmap);
            width = bitmap.Width;
            height = bitmap.Height;
            binImage1 = new Image<Gray, Byte>(bwImage.Bitmap).ThresholdBinary(new Gray(70.0), new Gray(255.0));
        }
        private void SecondImage(string filepath)
        {
            Bitmap bitmap;
            if (!String.IsNullOrEmpty(filepath))
            {
                bitmap = new Bitmap(filepath);
            }
            else
            {
                bitmap = new Bitmap(Properties.Resources.IMG_2050_min_edit);
            }
            var bwImage = new Image<Gray, Byte>(bitmap);
            width = bitmap.Width > width ? bitmap.Width : width;
            height = bitmap.Height > height ? bitmap.Height : height;
            binImage2 = new Image<Gray, Byte>(bwImage.Bitmap).ThresholdBinary(new Gray(70.0), new Gray(255.0));
        }

        private void ImagesFromConfigLoad()
        {
            string file = "config.xml";
            XElement conf;
            if (File.Exists(file))
                conf = XElement.Load(file);
            else
                conf = new XElement("images");

            List<XElement> images = conf.Elements().ToList();

            if (images.Count() == 2)
            {
                this.FirstImage(images[0].Attribute("path").Value);
                this.SecondImage(images[1].Attribute("path").Value);
            }
            else
            {
                this.FirstImage("");
                this.SecondImage("");
            }
        }

        private void ImagesFromFilesLoad()
        {
            var filenames = checkedListBoxFiles.CheckedItems;
            var path = tbFolderPath.Text;
            List<string> images = new List<string>();
            foreach (var filename in filenames)
            {
                images.Add(path + filename.ToString());
            }

            if (images.Count == 2)
            {
                this.FirstImage(images[0]);
                this.SecondImage(images[1]);
            }
        }

        private void ImagesLoad()
        {
            if (radioButtonConfig.Checked)
            {
                ImagesFromConfigLoad();
            }
            else
            {
                ImagesFromFilesLoad();
            }
        }

        private DataTable dt;
        private List<MyMatrix> myMatrixList = new List<MyMatrix>();
        
        private void GetDeformations_Click(object sender, EventArgs e)
        {
            this.ImagesLoad();

            if (binImage1 == null)
            {
                throw new Exception("first image null");
            }
            if (binImage2 == null)
            {
                throw new Exception("second image null");
            }

            var points1 = Workflow.GetPointsFromImage(binImage1, 1, imageBox1, imageBox2);
            var figureList1 = this.GetThreePointsFigures(points1);
            
            var points2 = Workflow.GetPointsFromImage(binImage2, 1, imageBox1, imageBox2);
            var figureList2 = this.GetThreePointsFigures(points2);

            var points1Count = points1.Count();
            var points2Count = points2.Count();

            if (points1Count != 20)
            {
                throw new Exception("!=20 points in first list");
            }
            if (points2Count != 20)
            {
                throw new Exception("!=20 points in second list");
            }
            
            if (points1Count != points2Count)
            {
                throw new Exception("неравное количество точек");
            }

            var deformationMatrixList = new List<Matrix<Single>>();
            var deformationTensorList = new List<Matrix<Single>>(); 
            
            
            MatrixDataTable mdt = new MatrixDataTable();
            var m = new MyMatrix();
            var figureCount = figureList1.Count();
            for (int i = 0; i < figureCount; i++)
            {
                var matOps = new MatrixOperations(new Points(figureList1[i], figureList2[i]));
                matOps.UltimateWorkflow();
                matOps.UltimateTest();
                m = matOps.GetMyMatrix();
                //  добавляем данные в список (нужно для реализации возможности отрисовки каждой матрицы)
                myMatrixList.Add(m);
                //  добавляем данные в таблицу
                mdt.AddRow(m);
                deformationMatrixList.Add(matOps.GetDeformationMatrix());
                deformationTensorList.Add(matOps.GetDeformationTensor());
            }
            dt = mdt.GetDataTable();
            
            foreach (DataRow row in dt.Rows)
            {
                listBox1.Items.Add(row["PId"].ToString());
            }

            Console.WriteLine("--------------DEF MATRIXES--------------");
            string text = this.ShowToConsole(deformationMatrixList);
            this.Text2File("defMatrixes", text);
            Console.WriteLine("--------------DEF TENSORS--------------");
            text = this.ShowToConsole(deformationTensorList);
            this.Text2File("defTensor", text);
            ExcelButton.Enabled = true;
        }

        public string ShowToConsole(List<Matrix<Single>> defMatList)
        {
            Single sum11 = 0, sum22 = 0;
            Single mean11, mean22;
            Int32 count = defMatList.Count();
            string text = "";
            for (var i = 0; i < count; i++)
            {
                ////Console.WriteLine("\n{0} element:", i);
                ////Console.WriteLine("\n{0}  {1}", defMatList[i][0, 0], defMatList[i][0, 1]);
                ////Console.WriteLine("\n{0}  {1}", defMatList[i][1, 0], defMatList[i][1, 1]);
                ////Console.WriteLine();
                text += "\n"+ i + " element:";
                text += "\n\n"+ defMatList[i][0, 0] +"  " + defMatList[i][0, 1];
                text += "\n\n" + defMatList[i][1, 0] + "  " + defMatList[i][1, 1];
                text += "\n";
                sum11 += defMatList[i][0, 0];
                sum22 += defMatList[i][1, 1];
            }
            mean11 = sum11 / count;
            mean22 = sum22 / count;
            text += "\nmean11 = " + mean11 + " mean22 = " + mean22;
            Console.Write(text);
            ////ResultLabel.Text = text;
            //////  Это дурацкий хак - убрать потом
            ////MinPixelsThresholdTextBox.Text = text;
            return text;
        }

        private void Text2File(string filename, string text)
        {
            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter(filename + ".txt");
            file.Write(text);

            file.Close();
        }

        ////private PointF FindCenter(List<Point> figure)
        ////{
        ////    var list = new List<CenterPoints>();
        ////    Single Xc = 0;
        ////    Single Yc = 0;
        ////    Int32 count = 0;
        ////    foreach (var point in figure)
        ////    {
        ////        Xc += point.X;
        ////        Yc += point.Y;
        ////        count++;
        ////    }
        ////    Xc /= count;
        ////    Yc /= count;
        ////    return new PointF(Xc, Yc);
        ////}

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int curItem = listBox1.SelectedIndex;
            if (dt != null && curItem < dt.Rows.Count)
            {
                DataRow dr = dt.Rows[curItem];
                HelpDrawing(dr, imageBox2);
            }
            //  включаем второй листбокс
            listBox2.Enabled = true;
            listBox3.Enabled = true;
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var steps = new Steps(width, height);
            steps.VisualizeEveryStep(myMatrixList[listBox1.SelectedIndex], listBox2.SelectedIndex, imageBox2);
        }

        private void HelpDrawing(DataRow dr, ImageBox imageBox)
        {
            var outfit = 10; //отступ

            var image = new Image<Bgr, byte>(width + outfit, height + outfit);
            image._Not();
            //  точки до изменений
            Single p1X = Convert.ToSingle(dr["p1X"]),
                   p1Y = Convert.ToSingle(dr["p1Y"]),
                   p2X = Convert.ToSingle(dr["p2X"]),
                   p2Y = Convert.ToSingle(dr["p2Y"]),
                   p3X = Convert.ToSingle(dr["p3X"]),
                   p3Y = Convert.ToSingle(dr["p3Y"]);
            Single radius = 8.0f;
            Int32 thickness = -1;
            
            var pointF = new PointF(p1X + outfit, p1Y + outfit);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Red), thickness);
            pointF = new PointF(p2X + outfit, p2Y + outfit);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Green), thickness);
            pointF = new PointF(p3X + outfit, p3Y + outfit);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Blue), thickness);
            
            //  точки после изменений
            pointF = new PointF(Convert.ToSingle(dr["p1Xnext"]) + outfit, Convert.ToSingle(dr["p1Ynext"]) + outfit);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.Orange), thickness);
            pointF = new PointF(Convert.ToSingle(dr["p2Xnext"]) + outfit, Convert.ToSingle(dr["p2Ynext"]) + outfit);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.GreenYellow), thickness);
            pointF = new PointF(Convert.ToSingle(dr["p3Xnext"]) + outfit, Convert.ToSingle(dr["p3Ynext"]) + outfit);
            image.Draw(new CircleF(pointF, radius), new Bgr(Color.DeepSkyBlue), thickness);

            var lines = new List<LineSegment2D>();
            lines.Add(new LineSegment2D(new Point(0, 10), new Point(image.Width - 1, 10)));
            lines.Add(new LineSegment2D(new Point(10, 0), new Point(10, image.Height - 1)));
            image.DrawLines(lines, 5);

            imageBox.Image = image;
        }

        private void MatTest_Click(object sender, EventArgs e)
        {
            ////var points1 = new List<Point> { new Point(4, 5), new Point(12, 13), new Point(7, 20) };
            ////var points2 = new List<Point> { new Point(8, 7), new Point(18, 11), new Point(17, 19) };
            ////var matOps = new MatrixOperations(new Points(points1, points2));
            ////matOps.UltimateWorkflow();
            ////var m = matOps.GetMyMatrix();
        }






        private void ExcelButton_Click(object sender, EventArgs e)
        {
            var mdt = new MatrixDataTable();
            mdt.ExportToExcel(dt, "excelworks.xls");
        }

        private void LoadFirstButton_Click(object sender, EventArgs e)
        {

        }

        private void LoadSecondButton_Click(object sender, EventArgs e)
        {

        }

        private ClusterList ClusterListFromImageWithThreshold(Image<Gray, byte> binImage)
        {
            var clusterList = new ClusterList(binImage);
            int threshold = Int32.Parse(MinPixelsThresholdTextBox.Text);
            clusterList.ExtractAllContours(threshold);
            return clusterList;
        }

        private void ShowClustersButton_Click(object sender, EventArgs e)
        {
            this.ImagesFromConfigLoad();
            int result1 = this.ClusterListFromImageWithThreshold(binImage1).AllContours.Count;
            int result2 = this.ClusterListFromImageWithThreshold(binImage2).AllContours.Count;
            ResultLabel.Text = "Result:" + " image1 - " + result1 + " image2 - " + result2;
        }

        private ClusterList ClusterListFromImage(Image<Gray, byte> binImage)
        {
            var clusterList = new ClusterList(binImage);
            clusterList.UltimateWorkflow();
            return clusterList;
        }

        private void DrawWithColorButton_Click(object sender, EventArgs e)
        {
            this.ImagesFromConfigLoad();
            var cList1 = this.ClusterListFromImage(binImage1);
            var cList2 = this.ClusterListFromImage(binImage2);

            var points1 = cList1.CenterPointsF;
            var points2 = cList2.CenterPointsF;

            var img1 = (new Image<Bgr, Byte>(binImage1.Width, binImage1.Height)).Not();
            var img2 = (new Image<Bgr, Byte>(binImage2.Width, binImage2.Height)).Not();

            for (int i = 0; i < points1.Count; i++)
            {
                ImageDrawingExtensions.CurrentColor = ImageDrawingExtensions.RandomColor();
                img1.DrawCenterPoint(points1[i]);
                img2.DrawCenterPoint(points2[i]);
            }

            imageBox1.Image = img1;
            imageBox2.Image = img2;
        }

        private void Matrix2Panel(Matrix<Single> mat)
        {
            ClearMatrixPanel();
            if (mat.Width <= 3 && mat.Height <= 3)
            {
                u11.Text = mat[0, 0].ToString();
                u21.Text = mat[1, 0].ToString();

                if (mat.Width == 1 && mat.Height == 2)
                {
                    return;
                }
                u12.Text = mat[0, 1].ToString();
                u22.Text = mat[1, 1].ToString();

                if (mat.Width == 2 && mat.Height == 2)
                {
                    return;
                }
                u13.Text = mat[0, 2].ToString();
                u23.Text = mat[1, 2].ToString();
                u31.Text = mat[2, 0].ToString();
                u32.Text = mat[2, 1].ToString();
                u33.Text = mat[2, 2].ToString();
            }
        }

        private void ClearMatrixPanel()
        {
            u11.Text = u12.Text = u13.Text = "";
            u21.Text = u22.Text = u23.Text = "";
            u31.Text = u32.Text = u33.Text = "";
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string textMatrixName = listBox3.Text;
            //TODO почему-то при старте программы один из пунктов меню выбран, это не желательно, т.к. из-за него текущее событие вызывается в ненужный момент
            if (listBox1.SelectedIndex < 0) return;
            MyMatrix mymat = myMatrixList[listBox1.SelectedIndex];
            Matrix<Single> mat = (Matrix<Single>) mymat.GetType().GetField(textMatrixName).GetValue(mymat);
            if (mat != null)
            {
                Matrix2Panel(mat);
            }
        }

        private void Copy2ClipboardButton_Click(object sender, EventArgs e)
        {
            string text = u11.Text + "\t" + u12.Text + "\t" + u13.Text + Environment.NewLine;
            text += u21.Text + "\t" + u22.Text + "\t" + u23.Text + Environment.NewLine;
            text += u31.Text + "\t" + u32.Text + "\t" + u33.Text + Environment.NewLine;
            Clipboard.SetText(text);
        }

        private void surfButton_Click(object sender, EventArgs e)
        {
            this.ImagesFromConfigLoad();
            long time;
            imageBox2.Image = SURFDetect.Draw(binImage1, binImage2, out time);
            timeLabel.Text = time.ToString();
        }

        private void radioButtonFiles_CheckedChanged(object sender, EventArgs e)
        {
            var radioButtonFiles = sender as RadioButton;
            if (radioButtonFiles.Checked)
            {
                checkedListBoxFiles.Enabled = true;
                TryFilledFilesList();
            }
            else
            {
                checkedListBoxFiles.Enabled = false;
            }
        }

        private void TryFilledFilesList()
        {
            string folderPath = tbFolderPath.Text;
            string[] files = Directory.GetFiles(@folderPath, "*.jpg");
            string filename;
            int index;
            foreach (var file in files)
            {
                index = file.LastIndexOf('\\');
                filename = file.Substring(index + 1);
                checkedListBoxFiles.Items.Add(filename);
            }
            for (int i = 0; i < checkedListBoxFiles.Items.Count; i++)
                checkedListBoxFiles.SetItemCheckState(i, CheckState.Unchecked);
            if (checkedListBoxFiles.Items.Count >= 2)
            {
                checkedListBoxFiles.SetItemChecked(0, true);
                checkedListBoxFiles.SetItemChecked(1, true);
            }
        }
    }
}

